from django.urls import path
from updates.api.views import UpdateModelDetailAPIView, UpdateModelListAPIView


urlpatterns=[
    path('', UpdateModelListAPIView.as_view()),
    path('<pk>/', UpdateModelDetailAPIView.as_view()),
]
