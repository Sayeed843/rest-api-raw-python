import requests, json



BASE_URL = "http://127.0.0.1:8000/"
END_POINT = "api/updates/"


def get_list():
    print("If you want to see all product list, just press a single space, then Enter.")
    print("For getting a specific product detail, please input the pk of product. It should be integer.")
    pk = input("Enter the pk of the product or a space: ")
    print(f'pk={pk}')
    if pk != " ":
        pk = int(pk)
        data ={'pk':pk}
    else:
        data={}
    r = requests.get(BASE_URL+END_POINT, data=json.dumps(data))
    print(f"Status Code={r.status_code}")
    if r.status_code != 200:
        print('Probably not good sign?')
    return r.json()


def create_update():
    user = int(input('Enter user ID: '))
    new_content = input('Enter the content: ')
    new_data = {
        'user':user,
        'content':new_content,
    }

    r = requests.post(BASE_URL+END_POINT, data=json.dumps(new_data))
    print(f"Status Code={r.status_code}")
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text


def do_obj_update():
    pk= int(input('Enter the pk of your prodcut: '))
    new_content = input("Please enter the new content: ")
    new_data = {
        'pk':pk,
        'content': new_content,
    }

    r = requests.put(BASE_URL+END_POINT, data=json.dumps(new_data))
    print(f"Status Code={r.status_code}")
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text


def do_obj_delete():
    pk= int(input('Enter the pk of your prodcut: '))
    new_data = {
        'pk':pk,
    }

    r = requests.delete(BASE_URL+END_POINT, data= json.dumps(new_data))
    print(f"Status Code={r.status_code}")
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text


if __name__ == "__main__":

    while True:
        print('\n')
        print("Please choice your item")
        print("1) Get The API List")
        print("2) Create A Item")
        print("3) Update The Item")
        print("4) Delete The Item")
        print("For leave type any other keys.")

        value = input("Your item number- ")

        if value == "1":
            print(get_list())
        elif value == "2":
            print(create_update())
        elif value == "3":
            print(do_obj_update())
        elif value == "4":
            print(do_obj_delete())
        else:
            break
